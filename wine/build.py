#!/usr/bin/env python3


import argparse
import os
import subprocess

from pathlib import PosixPath


def main() -> None:
    parser = argparse.ArgumentParser(prog="buildtools")

    # Positionnal arguments
    parser.add_argument("destdir", metavar="DESTDIR")

    # Optionnal arguments
    parser.add_argument("-C", "--srcdir", default=PosixPath.cwd())
    parser.add_argument("-b", "--builddir")
    parser.add_argument("-t", "--threads", metavar="N", default=os.cpu_count(), type=int)
    parser.add_argument("--no-distclean", action="store_false", dest="distclean")

    args = parser.parse_args()

    dest_path = PosixPath(args.destdir).resolve()
    src_path = PosixPath(args.srcdir).resolve()
    if args.builddir:
        build_path = PosixPath(args.builddir)
    else:
        build_path = src_path.parent.joinpath("build")

    build_path.mkdir(exist_ok=True)

    try:
        subprocess.run(
            [
                src_path.joinpath("configure"),
                f"--prefix={dest_path}",
                "--enable-archs=i386,x86_64",
                "--disable-tests"
            ],
            cwd=build_path,
            check=True,
        )

        subprocess.run(["make", f"-j{args.threads}", "-s"], cwd=build_path, check=True)
        subprocess.run(["make", "install", "-s"], cwd=build_path, check=True)
    except FileNotFoundError as e:
        print(f"{e.filename}: No such file or directory")
    except subprocess.CalledProcessError:
        pass
    else:
        cp = subprocess.run(
            ["git", "describe", "--tags", "--long", "master"],
            capture_output=True,
            cwd=src_path,
            check=False,
        )
        if cp.returncode == 0:
            dest_path.joinpath("share", "wine", "version").write_bytes(cp.stdout)
    finally:
        if args.distclean:
            subprocess.run(["make", "distclean", "-s"], cwd=build_path, check=False)

if __name__ == "__main__":
    main()

